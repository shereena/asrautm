//
//  ShadowView.swift
//  AstraUTM
//
//  Created by 123 on 25/10/21.
//

import Foundation
import UIKit

class ShadowView: UIView {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initializeView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initializeView()
    }
    
    func initializeView() {
        self.layer.borderColor = UIColor.clear.cgColor
        self.layer.borderWidth = 1.0
      
        self.layer.masksToBounds = true
        self.layer.shadowRadius = 15
        self.layer.shadowOpacity = 5
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 10 , height:1)

    }
    
}
