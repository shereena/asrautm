//
//  RegisterVC.swift
//  AstraUTM
//
//  Created by 123 on 27/10/21.
//
import Foundation
import UIKit
class RegisterVC: UIViewController {
   let registerVM = RegisterViewModel()
    
    @IBOutlet weak var userNameTF: LeftImageTextField!
    @IBOutlet weak var passwordTF: LeftImageTextField!
    
    @IBOutlet weak var loginView: ShadowView!{
        didSet{
            loginView.layer.cornerRadius = 25
        }
    }
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var actionView: ShadowView!{
        didSet{
            actionView.layer.cornerRadius = 25
        }
    }
    @IBOutlet weak var textFieldView: ShadowView!{
        didSet{
            textFieldView.layer.cornerRadius = 30
        }
    }
    
    @IBOutlet weak var loginAction: UIButton!
    
    @IBAction func backAction(_ sender: Any) {
        self.dismiss(animated: false)
    }
    override func viewDidLoad() {
        
    }
    
    @IBAction func registerAction(_ sender: Any) {
        if let password = passwordTF.text ,
           let username = userNameTF.text {
            if(username == "") {
                errorLabel.isHidden = false
                errorLabel.text = USERNAME_ERROR
                return
            }
            else if(password == "") {
                errorLabel.isHidden = false
                errorLabel.text = PASSWORD_ERROR
                return
            } else {
                let isSuccess = registerVM.registerUser(userName: username, password: password)
                if(isSuccess) {
                    let storyboard = UIStoryboard(name: "Home", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                    vc.modalPresentationStyle = UIModalPresentationStyle.fullScreen
                    self.present(vc, animated: false, completion: nil)
                } else {
                    errorLabel.isHidden = false
                    errorLabel.text = SOMTHING_ERROR
                }
            }
            
        } else {
            errorLabel.isHidden = false
        }
       
    }
    
}
extension RegisterVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

}
