//
//  RegisterViewModel.swift
//  AstraUTM
//
//  Created by 123 on 27/10/21.
//

import UIKit
import CoreData
class RegisterViewModel {
    func registerUser(userName : String, password : String) ->Bool {
        let appDelegate =  UIApplication.shared.delegate as! AppDelegate

        let context = appDelegate.persistentContainer.viewContext

        let newUser = NSEntityDescription.insertNewObject(forEntityName: "Users", into: context)

        newUser.setValue(userName, forKey: USERNAME)
        newUser.setValue(password, forKey: PASSWORD)
        do {
            try context.save()
            print("user saved")
            return true
        } catch {
            print ( SOMTHING_ERROR + "in user registration")
            return false
        }

    }

    
}
      
