//
//  LoginVM.swift
//  AstraUTM
//
//  Created by 123 on 27/10/21.
//

import CoreData
import UIKit
class LoginVM {
    func CheckForUserNameAndPasswordMatch (userName : String, password : String) ->Bool
       {
           let appDelegate =  UIApplication.shared.delegate as! AppDelegate
           let managedContext = appDelegate.persistentContainer.viewContext
          
           let fetchRequest: NSFetchRequest<Users> = Users.fetchRequest()

           let firstAttributePredicate = NSPredicate(format: "password = %@", password)
           let secondAttributePredicate = NSPredicate(format: "username = %@", userName)
         do {
             fetchRequest.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [firstAttributePredicate, secondAttributePredicate])
             do {
                 let fetchRecult = try managedContext.fetch(fetchRequest)
                 let count = fetchRecult.count
                 if count > 0
                 {
                         return true   
                 }
                 else
                 {
                     return false
                 }
             } catch let error as NSError {
                 return false
             }
          }
      }
}
