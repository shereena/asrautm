//
//  LoginVC.swift
//  AstraUTM
//
//  Created by 123 on 25/10/21.
//

import Foundation
import UIKit
class LoginVC: UIViewController {
    var loginVM = LoginVM()
    @IBOutlet weak var userNameTF: LeftImageTextField!
    @IBOutlet weak var passwordTF: LeftImageTextField!
    
    @IBOutlet weak var registerView: ShadowView!{
        didSet{
            registerView.layer.cornerRadius = 25
        }
    }
    @IBOutlet weak var actionView: ShadowView!{
        didSet{
            actionView.layer.cornerRadius = 25
        }
    }
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var textFieldView: ShadowView!{
        didSet{
            textFieldView.layer.cornerRadius = 30
        }
    }
    
  
    @IBAction func registerAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "RegisterVC") as! RegisterVC
        vc.modalPresentationStyle = UIModalPresentationStyle.fullScreen
        self.present(vc, animated: false, completion: nil)
    }
    
    @IBAction func loginAction(_ sender: Any) {
        if let password = passwordTF.text ,
           let username = userNameTF.text {
            if(username == "") {
                errorLabel.isHidden = false
                errorLabel.text = USERNAME_ERROR
                return
            }
            else if(password == "") {
                errorLabel.isHidden = false
                errorLabel.text = PASSWORD_ERROR
                return
            } else {
                let isSuccess = loginVM.CheckForUserNameAndPasswordMatch(userName: username, password: password)
                if(isSuccess) {
                    let storyboard = UIStoryboard(name: "Home", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                    vc.modalPresentationStyle = UIModalPresentationStyle.fullScreen
                    self.present(vc, animated: false, completion: nil)
                } else {
                    errorLabel.isHidden = false
                    errorLabel.text = WRONG_USERNAME_PASSWORD
                }
            }
            
        } else {
            errorLabel.isHidden = false
        }
       
    }
    
}
extension LoginVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

}
