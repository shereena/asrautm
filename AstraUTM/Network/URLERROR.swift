//
//  URLERROR.swift
//  AstraUTM
//
//  Created by 123 on 27/10/21.
//

import Foundation
fileprivate extension String {
    static let invalidURL = "URL is not valid"
}
enum URLError: Error {
    
    case urlMalformatted
    var value: String {
        switch self {
        case .urlMalformatted:
            return .invalidURL
        }
    }
}
