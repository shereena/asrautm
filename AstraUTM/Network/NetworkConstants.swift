//
//  NetworkConstants.swift
//  AstraUTM
//
//  Created by 123 on 27/10/21.
//

import Foundation
enum NetworkConstants: Int {
    case code = 200
    case failure = 400
    
    var value: Int {
        return self.rawValue
    }
}
