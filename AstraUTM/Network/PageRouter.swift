//
//  PageRouter.swift
//  AstraUTM
//
//  Created by 123 on 27/10/21.
//

import Alamofire
import UIKit

enum PageRouter: URLRequestConvertible {
   
   
    case weatherInfoList([String: Any])
    
    var method: HTTPMethod {
        switch self {
        case .weatherInfoList:
            return .get
       
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let encoding: ParameterEncoding = {
            switch self {
            case
                
               .weatherInfoList:
                return URLEncoding.default
           
            }
        }()
        let params: ([String: Any]?) = {
                   switch self {
                   case
                    .weatherInfoList(let params):
//                       print("aaaa", params)
                       return params
                   }
        }()
               
        
        let headers: HTTPHeaders = {
            switch self {
            case
                .weatherInfoList:
                  return [
                    "accept": "application/json",
                "Content-Type": "application/json"
                
            ]
            }
        }()
        
        let url: URL? = {
            let relativePath: String?
            
            var url = URL(string: WEATHERURL)
           
//         print(url)
            return url
        }()
        
        guard let formedURL = url else {
            throw URLError.urlMalformatted
        }
        var urlRequest = URLRequest(url: formedURL)
        urlRequest.httpMethod = method.rawValue
        urlRequest.allHTTPHeaderFields = headers
        
        return try encoding.encode(urlRequest, with: params)
    }
}




