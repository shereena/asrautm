//
//  HomeVC.swift
//  AstraUTM
//
//  Created by 123 on 27/10/21.
//

import Foundation
import UIKit
import GoogleMaps
class HomeVC: UIViewController , CLLocationManagerDelegate {
    
    @IBOutlet weak var faranBtn: UIButton!{
        didSet{
            faranBtn.tag = 1
        }
    }
    @IBOutlet weak var centigradeBtn: UIButton!{
        didSet{
            centigradeBtn.tag = 0
        }
    }
    @IBOutlet weak var tempTF: UILabel!
    @IBOutlet var MapView: GMSMapView!
    @IBOutlet weak var humidityTF: UILabel!
    @IBOutlet weak var windTF: UILabel!
    @IBOutlet weak var pressureTf: UILabel!
    @IBOutlet weak var placeTF: UILabel!
    @IBOutlet weak var weatherStatusTF: UILabel!

    var locationManager = CLLocationManager()
    var locatioAarray: [CLLocationCoordinate2D] = []
    var weatherVM = HomeViewModel()
    var weatherModel: ForecastModel?
    var isCentigrade  = true
    var degreeTemp = 0.0
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeTheLocationManager()
        self.MapView.isMyLocationEnabled = true
        self.MapView.delegate = self
        
    }

    func initializeTheLocationManager() {
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {

        let location = locationManager.location?.coordinate
        getWheatherInformation(latitude: "\(location!.latitude)",  longitude: "\(location!.longitude)")
        let center = CLLocationCoordinate2DMake(location!.latitude, location!.longitude)
        let coordinate1 = CLLocationCoordinate2DMake(center.latitude + 0.001, center.longitude + 0.001)
        let coordinate2 = CLLocationCoordinate2DMake(center.latitude + 0.006, center.longitude + 0.001)
        let coordinate3 = CLLocationCoordinate2DMake(center.latitude + 0.006, center.longitude + 0.003)
        
        let coordinate4 = CLLocationCoordinate2DMake(center.latitude + 0.001, center.longitude + 0.003)
        let coordinate5 = CLLocationCoordinate2DMake(center.latitude - 0.003, center.longitude + 0.002)
                
        locatioAarray = [coordinate1, coordinate2, coordinate3, coordinate4, coordinate5, coordinate1]
        drawPolygon()
        cameraMoveToLocation(toLocation: location)

    }

    func cameraMoveToLocation(toLocation: CLLocationCoordinate2D?) {
        if toLocation != nil {
            MapView.camera = GMSCameraPosition.camera(withTarget: toLocation!, zoom: 15)
        }
    }
    
    func drawPolygon() {
        MapView.clear()
        let path = GMSMutablePath()
        for point in locatioAarray {
            path.add(point)
        }
        let polyline = GMSPolyline(path: path)
        polyline.strokeColor = .blue
        polyline.strokeWidth = 5.0
        polyline.map = MapView
    }
    private func getWheatherInformation(latitude: String, longitude: String) {
        self.weatherVM.getWeather(latitude: latitude, longitude: longitude) {
            isSuccess, errorMessage  in
            
            self.weatherModel = self.weatherVM.responseStatus
            if let isSuccess = self.weatherModel?.cod {

                if( isSuccess  == "200" ){
                 if let temp = self.weatherModel?.list?[0].main?.temp {
                     self.degreeTemp = temp
                     self.tempTF.text =  String(format:"%.2f", self.degreeTemp) +   " °C"
                 }
                 self.weatherStatusTF.text = self.weatherModel?.list?[0].weather?[0].main ?? ""

                 if let pressure = self.weatherModel?.list?[0].main?.grndLevel {
                     self.pressureTf.text = "Pressure   :" + "\(pressure)" + " hpa"
                 }
                 if let city = self.weatherModel?.city?.name,
                    let country = self.weatherModel?.city?.country{
                     self.placeTF.text = city + ", " + country
                 }
                 if let speed = self.weatherModel?.list?[0].wind?.speed , let degree = self.weatherModel?.list?[0].wind?.deg {

                     self.windTF.text = "Wind   :" + "\(speed)" + "m/s(" + "\(degree)" + "°)"
                 }
                 if let humidity = self.weatherModel?.list?[0].main?.humidity {
                     self.humidityTF.text = "Humidity   :" + "\(humidity)" + "%"
                 }
                }

            }
        }
    }
    
    @IBAction func changeTempUnit(_ sender: UIButton) {
        if(sender.tag == 0) {
            faranBtn.setImage(UIImage(systemName: "circle"), for: .normal)
            centigradeBtn.setImage(UIImage(systemName: "circle.circle.fill"), for: .normal)
            
            self.tempTF.text = String(format:"%.2f", degreeTemp) +   " °C"
            isCentigrade = true
        } else {
            centigradeBtn.setImage(UIImage(systemName: "circle"), for: .normal)
            faranBtn.setImage(UIImage(systemName: "circle.circle.fill"), for: .normal)
            let faranTemarature = (degreeTemp * 9/5) + 32
            self.tempTF.text = String(format:"%.2f", faranTemarature)  + " F"
            isCentigrade = false
        }
        
    }
    

}
extension HomeVC: GMSMapViewDelegate {

  func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D)
    {
        var distances = [CLLocationDistance]()
        var getLat: CLLocationDegrees = coordinate.latitude
          var getLon: CLLocationDegrees = coordinate.longitude


          var getLocTouched: CLLocation =  CLLocation(latitude: getLat, longitude: getLon)
        for location in locatioAarray{
            let coord = CLLocation(latitude: location.latitude, longitude: location.longitude)
                   distances.append(coord.distance(from: getLocTouched))
            }

        let closest = distances.min()//shortest distance
        if let position = distances.index(of: closest!) {
            locatioAarray[position] = coordinate
            drawPolygon()
        }
        
    }

  }
extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
