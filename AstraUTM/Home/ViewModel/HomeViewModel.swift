//
//  HomeViewModel.swift
//  AstraUTM
//
//  Created by 123 on 27/10/21.
//

import Alamofire
import UIKit
class HomeViewModel {
    fileprivate let client = BaseAPIClient()
        var responseStatus: ForecastModel?
    func getWeather(latitude: String, longitude: String , onCompletion: @escaping (_ isSuccess: Bool, _ errorMessage: (ResponseError?)) -> Void) {
        var paramDict = [String: Any]()
        paramDict = ["appid": WEATHERAPPID, "cnt": "1" , "units": "metric", "lat": latitude, "lon": longitude]
        print(paramDict)
        client.makeNetworkRequest(requestObj: PageRouter.weatherInfoList(paramDict))  { (response: ForecastModel?, error) in
        
                 if let error = error { DispatchQueue.main.async {onCompletion(false, error)}; return }
                 self.responseStatus = response
                     DispatchQueue.main.async {onCompletion(false, nil)}
                     return
             
         }
         
     }
}
